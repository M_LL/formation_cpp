#include <iostream>
#include <functional>
#include <string>

#include "fonctions.h"

using namespace std;

unsigned getNbLivresRayon (unsigned longueurCm, unsigned etageres){
    return longueurCm * etageres / 3; //3 cm par livre
}

unsigned getNbLivresRayon (unsigned longueurCm, unsigned etageres, unsigned largeurLivre){
    return longueurCm * etageres / largeurLivre;
}
//SURCHARGE: plusieurs fonctions ont le m�me nom dans la m�me application
//nb d'arguments diff�rent: ok
//type diff�rent de variable (float, double...): pas ok, sauf si on cast le type ou .0
//type diff�rent de retour: pas ok

void montreOuvrage (string titre, string auteur, unsigned pages){
    cout << "**" << titre << "** de " << auteur << " (" << pages << " pages)" << endl;
}

void fonctionTest (string text){
 cout << "voila " << text << endl;
}



void parametres(){

    cout << "Param�tres de fonctions" << endl;
    cout << "4 m de long, 4 �tag�res : " << getNbLivresRayon(400,4) << " livres." << endl;
    cout << "4 m de long, 4 �tag�res et largeur custom : " << getNbLivresRayon(400,4,5) << " livres." << endl;

    montreOuvrage ("Le crayon abandonn�");
    montreOuvrage ("Le crayon abandonn�", "Yves Ciboulette");
    montreOuvrage ("Le crayon abandonn�", "Yves Ciboulette", 250);

}
/*
void variablesFonctions(){
    function<void(string, string, unsigned)> f1 =

}*/
