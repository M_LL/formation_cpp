#include <iostream>

#include "bases.h"

using namespace std;


void typesEtBoucles()
{
    cout << "truc utile : " << utile << endl; //n�cessite d'inclure bases.h pour utile

    cout << "Dans la m�diath�que on peut trouver toutes sortes d'ouvrages" << endl;
    //int ouvrages = 254000;
    //signed ouvrages = 254000;
    signed int ouvrages = 254'000; //' pour la lisibilit�
    unsigned dvds = 2'890;
    //int caisses = 9;
    //int caisses = 0b1001; //1001 = 9 en binaire
    int caisses = 011;
    //int caisses = 0x9; //hexa

    cout << "Taille d'un signed int : " << sizeof(ouvrages) << endl; //size en octets (4)
    char antennes = 8;
    short rayons = 100;
    int cds = 82300;
    long emprunts = 123'000;
    long long entrees = 340'120;
    int16_t auteurs = 1234;

    cout << "Taille char : " << sizeof(antennes) << endl; //1 octet
    cout << "Taille short : " << sizeof(rayons) << endl; //2 o
    cout << "Taille int : " << sizeof(cds) << endl; //4 o
    cout << "Taille long : " << sizeof(emprunts) << endl; //4 o
    cout << "Taille long long : " << sizeof(entrees) << endl; //8 o
    cout << "Taille int16_t : " << sizeof(auteurs) << endl; //2 o

    int16_t max_auteurs = INT16_MAX;
    float emprunts_par_jour = 4567.8f; //f pour d�clarer direct en float (sinon: d�clar� float puis converti en double)
    double emprunts_par_an = 345789.6;
    double emprunts_par_an2 = 3.457896e5;
    bool ouverte = true;

    string nom = "M�diath�que chez D�d�";

    rayons = ((rayons)%43); //>> 4;  ( % modulo)
    cout << "rayons = " << rayons << endl;
    bool assez_de_rayons = (rayons > 100)&&(rayons < 500);
    bool cd_fault = (cds<0) || (cds>1'000'000);
    bool cd_normaux = !cd_fault;
    bool un_nombre_rond = (cds==10'000)^(ouvrages == 100'000); //xor

    cds = cds + 1;
    cds += 1;
    cds++;
    int taille_bac = 870;
    int taille_cd = 11;
    int nb_cds_par_bac = taille_bac / taille_cd;

    cout << "870 / 11 = " << 870/11 << endl;
    cout << "Nombre de cds par bac = " << nb_cds_par_bac << endl;

    int bacs_cd_necessaires = cds / nb_cds_par_bac; //int: arrondi � l'inf�rieur

    bacs_cd_necessaires = (cds+nb_cds_par_bac-1)/(nb_cds_par_bac); //expliquer

    cout << "Nombre de bacs n�cessaires = " << bacs_cd_necessaires << endl;

    int espace_restant = taille_bac * bacs_cd_necessaires - cds;

    if (espace_restant >10) {
        cout << "on peut mettre aussi les 5 cassettes audio" << endl;

    }else
        cout << "On ne peut pas mettre les 5 cassettes audio" << endl;

    int camion_de_bac = bacs_cd_necessaires / 260;

    switch (bacs_cd_necessaires){
    case 0:
        cout << "D�m�nagementen voiture" << endl;
        break;
    case 1:
    case 2:
        cout << "un seul camion voire deux" << endl;
        break;
    default:
        cout << "plusieurs camions" << endl;
    }

    unsigned d = 0;
    while(d<bacs_cd_necessaires){
        //cout << "Bac CD n�" << d << endl;
        d += 100;
    }

    d = 0; //do while: effectue au moins un tour de boucle. While: sort de la boucle direct si la condition est fausse.
    do {
        cout << "Bac CD n�" << d << endl;
        d -= 200;
    } while (d<bacs_cd_necessaires);


    for (d=0; d<bacs_cd_necessaires; d+=300){
        cout << "Bac CD n�" << d << endl;
    }

    espace_restant = 14;

    for (int i=0, j=0; i<espace_restant; i+=j, j++){
        cout << "Cassette : " << i << endl;
    }

    //float usagers_par_jour0[7];
    //float usagers_par_jour1[3+2*2];
    int jours = 7;
    float usagers_par_jour[jours];
    //const int JOURS = 7;
    //float usagers_par_jour3[JOURS];
    //float usagers_par_jour4[JOURS*2-7];

    usagers_par_jour[0] = 125.98f;
    //usagers_par_jour[200] = 90;
    float usagers_weekend[] {320.5f, 0}; //initialisation partielle
    float usagers_par_demi_jour[7][2]; //7*2*4=56 octets;
    usagers_par_demi_jour[6][1] = 0; //dimanche aprem

    //for (float uj : usagers_par_jour)
    //    cout << uj << " ";

    int livres_par_rayon[]{1325,3547,3213,123,4861,566,14354,135,456,1344,4355};

    int rayon_min = livres_par_rayon[0];
    int rayon_max = 0;
    int rayon_min_index = 0;
    int rayon_max_index = 0;

    cout << "sizeof(livres_par_rayon)/sizeof(int)" << sizeof(livres_par_rayon)/sizeof(int) << endl;

    for (int i=0; i<(sizeof(livres_par_rayon)/sizeof(int)); i++){
        cout << i << endl;
        if (livres_par_rayon[i]<rayon_min){
            rayon_min = livres_par_rayon[i];
            rayon_min_index = i;
            cout << "rayon min" << rayon_min << endl;
        }
        if (livres_par_rayon[i] > rayon_max){
            rayon_max = livres_par_rayon[i];
            rayon_max_index = i;
            cout << "rayon max" << rayon_max << endl;
        }

    }

    //retourner le num�ro du rayon et non sa quantit� de livres
    cout << "Rayon avec le moins de livres : " << rayon_min_index << endl;
    cout << "Rayon avec le plus de livres : " << rayon_max_index << endl;

}

